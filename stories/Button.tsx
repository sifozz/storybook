import React from 'react';
import './button.css';

interface ButtonProps {
  /**
   * Is this the principal call to action on the page?
   */
  primary?: boolean;
  /**
   * What background color to use
   */
  backgroundColor?: string;
  /**
   * How large should the button be?
   */
  size?: 'small' | 'medium' | 'large';
  /**
   * Button contents
   */
  label: string;
  /**
   * Optional click handler
   */
  onClick?: () => void;
}

/**
 * Primary UI component for user interaction
 */
export const Button = ({
  primary = false,
  size = 'medium',
  backgroundColor,
  label,
  ...props
}: ButtonProps) => {
  const classes = ['font-sans font-bold border-0 rounded-md cursor-pointer inline-block'];

  classes.push(primary ? 'bg-blue-500 text-white' : 'bg-transparent text-gray-600 shadow-md');

  switch (size) {
    case 'small':
      classes.push('text-xs px-4 py-2.5');
      break;
    case 'large':
      classes.push('text-base px-6 py-3');
      break;
    default:
      classes.push('text-sm px-5 py-2.5');
  }

  return (
    <button
      type="button"
      className={classes.join(' ')}
      {...props}
    >
      {label}
      <style jsx>{`
        button {
          background-color: ${backgroundColor};
        }
      `}</style>
    </button>
  );
};
